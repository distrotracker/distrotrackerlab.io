---
title: "Distro Tracker PWA"

layout: post
categories: news
date: 2024-11-11 10:00:00 +0200
lang: en

author: Frédéric
---

Continuing the <b>development of tools using Distro Tracker data</b>, I am presenting today a <b>PWA version</b>.

![pwa](/assets/img/pwa_2.png){: .img-fluid .border .border-primary }

The PWA technology, now almost 10 years old, has a rather uncertain future: viewed unfavorably by those promoting their app stores or overlooked by certain browsers. **Distro Tracker PWA is mainly designed to allow you to access data offline.**

### Installation.

![pwa](/assets/img/pwa_1.png){: .img-fluid .border .border-primary }

Simply go to the **[Webdatas](/en/datas.html)** page, and installation will be suggested in compatible browsers. You will then find a "Distro Tracker" icon in your app list.

![pwa](/assets/img/pwa_3.png){: .img-fluid .border .border-primary }

**Distro Tracker PWA is therefore a new way to monitor GNU/Linux & BSD distributions, using data directly from Distro Tracker.**

**Happy tracking !**