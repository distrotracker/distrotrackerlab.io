---
title: "Distro Widget, to always have Distro Tracker close at hand !"

layout: post
categories: news
date: 2024-10-20 10:00:00 +0200
lang: en

author: Frédéric
---

Today, I am introducing a new simple, user-friendly, and practical tool: **Distro Widget** !

It is a **special configuration for [Conky](https://conky.cc/documents/about)**, the system monitor that displays in the background on the desktop, compatible with GNU/Linux & BSD systems.

What's special about it ? **It integrates data from Distro Tracker & Distro Reporter !** Yes, for the first time, I'm developing a "desktop" tool linked to Distro Tracker.

After a **simple installation**, **distro-get**, a small independent and lightweight utility, is added to the `/home/user/.config/conky/` directory to provide two features: **downloading data** (by default, the data updated every morning on my Gitlab in JSON format), and **reading this data**. The Conky configuration itself is located in the `conky.conf` file, alongside a header image for [Conky](https://conky.cc/documents/about). A `conky.desktop` file is also created in the `/home/user/autostart/` directory.

For now, in addition to some system information and statistics, it randomly displays a distribution along with the date and version number of its latest release, as well as the list of distributions that have had changes in the last downloaded report.

### Installation.

```
wget https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-widget/dev/standalone/distro-widget.sh --no-check-certificate && chmod +x distro-widget.sh && ./distro-widget.sh --username "$USER"
```

### Configuration.

Daily download of remote data, for example via crontab, with the default JSON format :

```
0 13 * * * /home/test/.config/conky/distro-get.sh --action download --type tracker
0 13 * * * /home/test/.config/conky/distro-get.sh --action download --type reporter
```

### Example.

![distro-widget example]({{ site.url }}/assets/img/distro-widget.png){: .img-fluid }

Feel free to share your feedback. A Debian & Snapcraft package will be created soon.

**Happy tracking !**