---
title: "Distro Tracker 1.1.1 - bugfix release"

layout: post
categories: news
date: 2024-10-01 17:07:00 +0200
lang: en

author: Frédéric
---

Distro Tracker is now in 1.1.1, a bugfix release for the following issues :

- Fixed **dependency 'yq' (from Mike Farah) in Snap package**.
- Fixed the regular expression associated with **Linux Kernel** information.
- Added **additional protection** that invalidates any result with two entities in the response.

Update your scripts, Debian and Snap packages to take advantage of these fixes.

Stay tuned because **in a few days _Distro Reporter_ will be released in its first stable version 1.0.0**, a "little brother" for _Distro Tracker_, in order to independently analyze and manage the results output by _DT_. The '--report' option will then be removed and replaced with this brand new family member.

Join **the global community of Distro Tracker users** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Happy tracking !**