---
title: "World - IT outage : issue at Crowdstrike"

layout: post
categories: news
date: 2024-07-19 17:07:00 +0200
lang: en

author: Frédéric
---

#crowdstrike #windows - Good news ! The 22 GNU/Linux & BSD distributions listed in the Distro Tracker monitoring tool are not affected by this issue !

IT professionals are directly concerned with **issues of sovereignty, performance, and efficiency**. Tools that address these concerns are always welcome.

Join **the global community of Distro Tracker users**, the only open-source script that provides the latest stable versions of 22 GNU/Linux & BSD distributions, along with their download links, in multiple output formats.

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Happy tracking !**