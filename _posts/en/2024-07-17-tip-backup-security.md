---
title: "Tip : --backup option for backup security"

layout: post
categories: news
date: 2024-07-17 18:07:00 +0200
lang: en

author: Frédéric
---

# Use the --backup option.

By adding the _--backup_ (or _-b_ in simplified notation) option to the command, you protect yourself against an unexpected event independent of Distro Tracker : the inaccessibility of the website of one of the distributions from which you wanted to retrieve information.

# Option mechanism.

Depending on the chosen output file name, either customized or date-based, the script will test and find the most recently preserved old data and restore it in your new output file.

# Data processing.

Due to the _source_ field present in all output file formats, the script notifies you whether the data is new (_current_) or from previous data (_archive_).

This way, you can always and under all circumstances create your data file !

**Happy tracking !**