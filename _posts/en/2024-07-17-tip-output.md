---
title: "Tip : --output option for data naming"

layout: post
categories: news
date: 2024-07-17 19:07:00 +0200
lang: en

author: Frédéric
---

# Using the --output option.

By using this option, you will get a file name of the type _myfile.current.csv_, but if you do not use it, you will get a file name of the type _DT-date.csv_.

# Option mechanism.

Depending on the chosen output file name, in "custom" mode, the script will create a maximum of two files: _current_ and _archive_. In "date" mode, the script will create a new file each time the script is executed.

For use this option, set a file path without extension.

**Happy tracking !**