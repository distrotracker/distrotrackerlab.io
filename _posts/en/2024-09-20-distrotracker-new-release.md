---
title: "New release Distro Tracker 1.1.0"

layout: post
categories: news
date: 2024-09-20 17:07:00 +0200
lang: en

author: Frédéric
---

Distro Tracker evolves and moves to version 1.1.0 !

Here are the new features of this version, which adds several functionalities and fixes some bugs :

- Increased to **25 distributions** with the following new entries : Linux (Kernel), Nextcloud (Server) & Rescuezilla.
- New **documentation**, with a man file following the standards.
- New **NOK filter** for OPNsense, and new **URLs** for openSUSE & Mageia.
- Added **YAML format** as output, along with the addition of the '--format' option to choose between CSV, JSON, or YAML - by default, only CSV is output.
- The default value for the '--length' option is now '3'.
- All options are lowercase.
- Revision dates in JSON & YAML outputs, and in the report, are in **standard ISO 8601 format**.
- Fixed execution time calculation.

These are not the only new features for Distro Tracker, as changes have also been made to the website :

- Highlight of the latest news and the latest alert report on the homepage.
- Added a "Roadmap" page to learn more about the development of _distro-tracker_ and its utilities.
- The site is now trilingual with the addition of a Spanish translation !

And finally, the addition of the **.deb format (Debian package)**, which complements the existing distribution formats (Bash source, Bash binary & Snapcraft package).

Join **the global community of Distro Tracker users** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Happy tracking !**