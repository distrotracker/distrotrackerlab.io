---
title: "Distro Tracker 1.0 release official announcement"

layout: post
categories: news
date: 2024-07-17 17:07:00 +0200
lang: en

author: Frédéric
---

# Distro Tracker 1.0 is here !

After several weeks of development and a rebranding, **here is Distro Tracker version 1.0**, the monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions, along with their download links.

First and foremost, and the most visible change, is **the creation of a dedicated website : https://distrotracker.com**. This bilingual site includes an HTML rendering page of the collected data, a feed of changes to this data, as well as articles on the detailed operation of the program.

You will find the original Bash script there, but now also available **in multiple distribution modes** : a Snap package, a Flatpak package, and a PowerShell module !

**Here are the new features** :

- Expansion to **22 GNU/Linux** with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, & XCP-ng.
- General mechanism for retrieving version numbers using the **_-LENGTH_ parameter** has been revised for more logic : not just X tested versions are returned, but now X valid versions.
- Addition of the **_-FILE_ parameter** to choose tracking URLs via a local or remote JSON file.
- Addition of the **_-REPORT_ parameter** to generate a JSON report to notify of any changes in the latest release of each distribution.
- Activation of the **_-ENV_ parameter**.
- Renaming of the _-DISTRIB_ parameter to _-DISTRO_, _-DEPTH_ to _-LENGTH_, _-DATASECURITY_ to _-BACKUP_.
- Addition of one-letter shortcuts for each parameter and option.
- Addition of the **Compare_NokFilter function** to better manage and clarify versions tagged "NOK" (beta, RC, etc.) : all pingable versions are retrieved, with a final filter on stable versions.

**Happy tracking !**