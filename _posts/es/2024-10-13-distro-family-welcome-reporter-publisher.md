---
title: "¡ Distro Reporter y Distro Publisher llegan a la familia !"

layout: post
categories: news
date: 2024-10-13 10:00:00 +0200
lang: es

author: Frédéric
---

¡ Distro Tracker da la bienvenida hoy a dos hermanitos: **Distro Reporter** y **Distro Publisher** !

Pero antes que nada, **déjenme agradecerles por sus numerosos comentarios**, y gracias a ustedes, Distro Tracker ha superado los 50 usuarios semanales según las estadísticas oficiales de Snapcraft. ¡ Gracias a todos y todas !

En Distro Tracker, la funcionalidad de gestión de diferencias entre dos conjuntos de datos recolectados estaba **hasta ahora incluida directamente en Distro Tracker**, pero esto limitaba las posibilidades. A partir de ahora, **son libres de programar como deseen la comparación entre datos**, y generar informes en formatos JSON o YAML.

![distro-family]({{ site.url }}/assets/img/distro-family.png){: .img-fluid }

**Distro Reporter** ya está disponible en su **primera versión estable 1.0.0**, lo que para mí significa la adición de todas las funcionalidades previstas desde el principio. El script está disponible en los formatos **fuente Bash**, **binario Bash**, **paquete Debian** y **paquete Snap**. Pueden encontrar todo en mi Gitlab **[aquí](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-reporter/prod/)**. Esta funcionalidad será eliminada pronto de Distro Tracker.

**Distro Publisher** también está en su **primera versión estable 1.0.0**. Esta vez, se trata de poder publicar los informes de cambios generados por **Distro Reporter** en diferentes plataformas o redes sociales. Así, se cierra el ciclo que comienza con la recolección de datos, seguido por el análisis de los cambios y finalmente la publicación en distintos medios.

Aprovecho para anunciarles también **la apertura de [la página de Facebook de Distro Tracker](https://www.facebook.com/distrotracker)**, con la publicación automática de los informes generados y las novedades del sitio. ¡ No duden en suscribirse y darle like !

Otros herramientas dedicadas a la gestión de contenido descargable serán desarrolladas más adelante. Visiten la página **[Roadmap]({{ site.url }}/es/roadmap.html)** para descubrir las futuras evoluciones planeadas.

Y si aún no lo han hecho: ¡ únanse a **la comunidad mundial de usuarios de Distro Tracker** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**¡ Bueno rastreo !**