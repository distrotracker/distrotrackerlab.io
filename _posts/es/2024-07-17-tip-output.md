---
title: "Consejo : --output opción para nombrar datos"

layout: post
categories: news
date: 2024-07-17 19:07:00 +0200
lang: es

author: Frédéric
---

# Utilizar la opción --output.

Al usar esta opción, obtendrá un nombre de archivo del tipo _monfichier.current.csv_, pero si no la utiliza, obtendrá un nombre de archivo del tipo _DT-date.csv_.

# Mecanismo de la opción.

Según el nombre de archivo de salida elegido, en modo "personalizado" el script creará como máximo dos archivos _current_ y _archive_, y en modo "fecha" el script creará un nuevo archivo en cada ejecución del script.

Para utilizar esta opción, establezca una ruta de archivo sin extensión.

**¡ Bueno rastreo !**