---
title: "Mundo - Avería informática : problema en Crowdstrike"

layout: post
categories: news
date: 2024-07-19 17:07:00 +0200
lang: es

author: Frédéric
---

#crowdstrike #windows - ¡Buenas noticias! ¡Las 22 distribuciones GNU/Linux & BSD presentes en la herramienta de monitoreo informático Distro Tracker no se ven afectadas por este problema!

Los profesionales de la informática están directamente interesados en **los temas de soberanía, rendimiento y eficiencia**. Las herramientas que permiten abordar estos problemas siempre son bienvenidas.

Únase a **la comunidad global de usuarios de Distro Tracker**, el único script open-source que permite conocer las últimas versiones estables de 22 distribuciones GNU/Linux & BSD, con sus enlaces de descarga, en varios formatos de salida de resultados.

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**¡ Bueno rastreo !**