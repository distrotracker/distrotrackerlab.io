---
title: "Distro Tracker 1.1.1 - versión de corrección"

layout: post
categories: news
date: 2024-10-01 17:07:00 +0200
lang: es

author: Frédéric
---

Distro Tracker ahora está en 1.1.1, una solución para los siguientes problemas :

- Se corrigió la **dependencia 'yq' (de Mike Farah) en el paquete Snap**.
- Se corrigió la expresión regular asociada con la información del **Kernel de Linux**.
- Se agregó **protección adicional** que invalida cualquier resultado con dos entidades en la respuesta.

Actualice sus scripts, paquetes Debian y Snap para aprovechar estas correcciones.

Estén atentos porque **en unos días se lanzará _Distro Reporter_ en su primera versión estable 1.0.0**, un "hermano pequeño" de _Distro Tracker_, con el fin de analizar y gestionar de forma independiente los resultados generados por _DT_. La opción '--report' se eliminará y se reemplazará con este nuevo miembro de la familia.

¡ Únete a **la comunidad global de usuarios de Distro Tracker** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**¡ Feliz seguimiento !**