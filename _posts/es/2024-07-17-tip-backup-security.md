---
title: "Consejo : --backup opción para la seguridad de los datos"

layout: post
categories: news
date: 2024-07-17 18:07:00 +0200
lang: es

author: Frédéric
---

# Utilizar la opción --backup.

Al añadir la opción _--backup_ (o _-b_ en notación simplificada) al comando, se protege contra un evento inesperado independiente de Distro Tracker : la inaccesibilidad del sitio web de una de las distribuciones de las que desea recuperar la información.

# Mecanismo de la opción.

Según el nombre de archivo de salida elegido, personalizado o basado en la fecha, el script buscará y recuperará los datos antiguos más recientemente conservados y los colocará en su nuevo archivo de salida.

# Procesamiento de datos.

Gracias al campo _source_ presente en todos los formatos del archivo de salida, el script le notificará si se trata de datos nuevos (_current_) o provenientes de datos anteriores (_archive_).

¡ Así podrá siempre y en cualquier circunstancia constituir su archivo de datos !

**¡ Bueno rastreo !**