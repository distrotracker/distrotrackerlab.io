---
title: "¡ Distro Widget, para tener siempre Distro Tracker a mano !"

layout: post
categories: news
date: 2024-10-20 10:00:00 +0200
lang: es

author: Frédéric
---

Hoy les presento una nueva herramienta simple, amigable y práctica: ¡ **Distro Widget** !

Se trata de **una configuración especial para [Conky](https://conky.cc/documents/about)**, el monitor del sistema que se muestra en segundo plano en el escritorio, compatible con los sistemas GNU/Linux y BSD.

¿ Su particularidad ? ¡ **Integra los datos de Distro Tracker y Distro Reporter !** Sí, por primera vez desarrollo una herramienta de uso "de escritorio" vinculada a Distro Tracker.

Después de una **instalación simple**, **distro-get**, un pequeño y liviano utilitario independiente, se agrega en el directorio `/home/user/.config/conky/` para proporcionarte dos funciones: la **descarga de datos** (por defecto, los actualizados cada mañana en mi Gitlab y en formato JSON) y la **lectura de estos datos**. La configuración de Conky se encuentra en el archivo `conky.conf`, junto con una imagen de cabecera para [Conky](https://conky.cc/documents/about). También se crea un archivo `conky.desktop` en el directorio `/home/user/autostart/`.

Por el momento, además de algunas informaciones del sistema y estadísticas, se muestra aleatoriamente una distribución con la fecha y número de su última versión, así como la lista de distribuciones que han tenido un cambio en el último informe descargado.

### Instalación.

```
wget https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-widget/dev/standalone/distro-widget.sh --no-check-certificate && chmod +x distro-widget.sh && ./distro-widget.sh --username "$USER"
```

### Configuración.

Descarga diaria de datos remotos, por ejemplo, con crontab, con el formato JSON por defecto :

```
0 13 * * * /home/test/.config/conky/distro-get.sh --action download --type tracker
0 13 * * * /home/test/.config/conky/distro-get.sh --action download --type reporter
```

### Ejemplo.

![distro-widget example]({{ site.url }}/assets/img/distro-widget.png){: .img-fluid }

No dudes en compartir tus comentarios. Próximamente se crearán paquetes para Debian y Snapcraft.

**¡ Bueno rastreo !**