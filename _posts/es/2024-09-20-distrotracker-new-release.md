---
title: "Nueva versión Distro Tracker 1.1.0"

layout: post
categories: news
date: 2024-09-20 17:07:00 +0200
lang: es

author: Frédéric
---

¡ Distro Tracker evoluciona y pasa a la versión 1.1.0 !

Aquí están las novedades de esta nueva versión, que añade varias funcionalidades y corrige algunos errores :

- Expansión a **25 distribuciones** con las siguientes nuevas entradas : Linux (Kernel), Nextcloud (Server) & Rescuezilla.
- Nueva **documentación**, con un archivo man que sigue los estándares.
- Nuevo **filtro NOK** para OPNsense, y nuevas **URLs** para openSUSE & Mageia.
- Se añadió el **formato YAML** como salida, junto con la opción '--format' para elegir entre CSV, JSON o YAML - por defecto, solo CSV se genera como salida.
- El valor por defecto de la opción '--length' es ahora '3'.
- Todas las opciones están en minúsculas.
- Las fechas de revisión en las salidas JSON y YAML, y en el informe, están en **formato estándar ISO 8601**.
- Corrección del cálculo del tiempo de ejecución.

Estas no son las únicas novedades para Distro Tracker, ya que también se han realizado cambios en el sitio web :

- Destacando las últimas noticias y el último informe de alerta en la página de inicio.
- Se ha añadido una página "Roadmap" para conocer más sobre el desarrollo de _distro-tracker_ y sus utilidades.
- ¡ El sitio ahora es trilingüe con la adición de una traducción al español !

Y por último, la adición del **formato .deb (paquete Debian)** que se suma a los formatos de distribución ya existentes (fuente Bash, binario Bash & paquete Snapcraft).

¡ Únete a **la comunidad global de usuarios de Distro Tracker** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**¡ Feliz seguimiento !**