---
title: "Anuncio oficial Distro Tracker 1.0"

layout: post
categories: news
date: 2024-07-17 17:07:00 +0200
lang: es

author: Frédéric
---

# ¡ Distro Tracker 1.0 ha llegado !

Después de varias semanas de desarrollo y un cambio de identidad, **llega la versión 1.0 de Distro Tracker**, la herramienta de monitoreo informático que permite conocer las últimas versiones estables de distribuciones GNU/Linux & BSD, junto con sus enlaces de descarga.

Primero y, evidentemente, lo más visible es **la creación de un sitio web dedicado : https://distrotracker.com**. Este sitio, bilingüe, incluye una página con la representación HTML de los datos recogidos, un flujo de cambios en esos datos, así como artículos sobre el funcionamiento detallado del programa.

Encontrarás el script Bash original, pero ahora también **bajo varios modos de distribución** : un paquete Snap, un paquete Flatpak y un módulo PowerShell.

**En cuanto a las funcionalidades, estas son las novedades** :

- Adición de **22 distribuciones** con los nuevos participantes : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware y XCP-ng.
- Revisión del mecanismo general de recuperación de números de versiones siguiendo el **parámetro _-LENGTH_** para mayor lógica : ahora no se devuelven solo X versiones probadas, sino X versiones válidas.
- Añadido del **parámetro _-FILE_** para elegir las URLs de seguimiento a través de un archivo JSON local o remoto.
- Añadido del **parámetro _-REPORT_** para generar un informe en formato JSON para notificar los posibles cambios en la última versión de cada distribución.
- Activación del **parámetro _-ENV_**.
- Renombramiento de los parámetros _-DISTRIB_ a _-DISTRO_, _-DEPTH_ a _-LENGTH_, _-DATASECURITY_ a _-BACKUP_.
- Añadido de accesos directos de una letra a cada parámetro y opción.
- Añadida una **función Compare_NokFilter** para gestionar y detallar mejor las versiones etiquetadas como "NOK" (beta, RC, etc.) : se recuperan todas las versiones disponibles, con un último filtro en las versiones estables.

**¡ Bueno rastreo !**