---
title: "1 semana después del lanzamiento de Distro Tracker 1.0 : ¡ gracias !"

layout: post
categories: news
date: 2024-07-23 17:07:00 +0200
lang: es

author: Frédéric
---

¡ Gracias por **su entusiasmo** y **sus comentarios** sobre el lanzamiento de Distro Tracker 1.0 ! Sus observaciones son muy importantes para mejorar el programa.

Como se puede ver en el mapa a continuación, Distro Tracker ahora se utiliza en varios países diferentes.

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

Una versión "bugfix", que abordará sus observaciones, está en desarrollo y se publicará en los próximos días.

Primero habrá **una nueva documentación**, siguiendo de cerca los estándares en la materia, con la adición de una página man adecuada. También se añadirá **el formato YAML** y la posibilidad de descargar los datos generados por distrotracker.com, en lugar de un seguimiento manual.

Luego, me centraré en la creación de un **paquete Flatpak** y un **paquete .deb** clásico para completar la lista de modos de distribución.

**¡ Bueno rastreo !**