---
title: "Distro Tracker PWA"

layout: post
categories: news
date: 2024-11-11 10:00:00 +0200
lang: es

author: Frédéric
---

Continuando con el <b>desarrollo de herramientas que utilizan los datos de Distro Tracker</b>, hoy les presento una <b>versión PWA</b>.

![pwa](/assets/img/pwa_2.png){: .img-fluid .border .border-primary }

La tecnología PWA, que ya tiene casi 10 años, tiene un futuro algo incierto: mal vista por quienes promueven sus tiendas de aplicaciones o ignorada por ciertos navegadores. **Distro Tracker PWA está diseñado principalmente para permitirte consultar los datos sin conexión.**

### Instalación.

![pwa](/assets/img/pwa_1.png){: .img-fluid .border .border-primary }

Simplemente ve a la página **[Webdatas](/es/datas.html)** y se te ofrecerá la instalación en navegadores compatibles. Luego encontrarás un ícono de "Distro Tracker" en tu lista de aplicaciones.

![pwa](/assets/img/pwa_3.png){: .img-fluid .border .border-primary }

**Distro Tracker PWA es una nueva forma de hacer seguimiento a las distribuciones GNU/Linux & BSD, usando directamente los datos de Distro Tracker.**

**¡ Bueno rastreo !**