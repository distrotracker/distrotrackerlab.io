---
title: "2024-11-29 (FreeBSD)"

layout: post
categories: changes
date: 2024-11-29 20:03:18 +0200

author: reporter
---

distribution freebsd - changes :
- last release upgrade from '14.1' to '14.2'
- last url from 'https://download.freebsd.org/releases/ISO-IMAGES/14.1/FreeBSD-14.1-RELEASE-amd64-dvd1.iso' to 'https://download.freebsd.org/releases/ISO-IMAGES/14.2/FreeBSD-14.2-RELEASE-amd64-dvd1.iso'

