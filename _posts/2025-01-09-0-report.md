---
title: "2025-01-09 (Linux Kernel)"

layout: post
categories: changes
date: 2025-01-09 20:03:30 +0200

author: reporter
---

distribution Linux Kernel - changes :
- last release upgrade from '6.12.8' to '6.12.9'
- last url from 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.12.8.tar.gz' to 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.12.9.tar.gz'

