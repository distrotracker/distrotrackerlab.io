---
title: "2024-12-15 (Linux Kernel)"

layout: post
categories: changes
date: 2024-12-15 12:03:06 +0200

author: reporter
---

distribution Linux Kernel - changes :
- last release upgrade from '6.12.4' to '6.12.5'
- last url from 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.12.4.tar.gz' to 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.12.5.tar.gz'

