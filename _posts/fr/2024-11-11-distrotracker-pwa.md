---
title: "Distro Tracker PWA"

layout: post
categories: news
date: 2024-11-11 10:00:00 +0200
lang: fr

author: Frédéric
---

Poursuivant le <b>développement d'outils utilisant les données de Distro Tracker</b>, je vous présente aujourd'hui une <b>version PWA</b>.

![pwa](/assets/img/pwa_2.png){: .img-fluid .border .border-primary }

La technologie PWA, bientôt âgée de 10 ans, a aujourd'hui son avenir assez incertain : mal vue par ceux mettant en avant leurs boutiques d'applications, ou délaissée par des navigateurs. **Distro Tracker PWA est principalement réalisé pour vous permettre de consulter les données hors connexion.**

### Installation.

![pwa](/assets/img/pwa_1.png){: .img-fluid .border .border-primary }

Allez simplement sur la page **[Webdatas](/fr/datas.html)** est l'installation vous sera proposée dans les navigateurs compatibles. Vous retrouverez alors une icône "Distro Tracker" dans votre liste d'applications.

![pwa](/assets/img/pwa_3.png){: .img-fluid .border .border-primary }

**Distro Tracker PWA est donc un nouveau moyen de réaliser votre veille sur les distributions GNU/Linux & BSD, en utilisant directement les données de Distro Tracker.**

**Bon tracking !**