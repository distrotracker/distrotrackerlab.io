---
title: "Monde - Panne informatique : problème chez Crowdstrike"

layout: post
categories: news
date: 2024-07-19 17:07:00 +0200
lang: fr

author: Frédéric
---

#crowdstrike #windows - Bonne nouvelle ! Les 22 distributions GNU/Linux & BSD présentes dans l’outil de veille informatique Distro Tracker ne sont pas touchées par ce problème !

Les professionnels de l’informatique sont directement concernés par **les problématiques de souveraineté, de performance et d’efficacité**. Les outils permettant d’y répondre sont toujours bienvenus.

Rejoignez **la communauté mondiale des utilisateurs de Distro Tracker**, le seul script open-source permettant de connaître les dernières versions stables de 22 distributions GNU/Linux & BSD, avec leurs liens de téléchargement, sous plusieurs formats de sortie en résultat.

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Bon tracking !**