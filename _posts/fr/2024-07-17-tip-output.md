---
title: "Astuce : option --output pour le nommage des données"

layout: post
categories: news
date: 2024-07-17 19:07:00 +0200
lang: fr

author: Frédéric
---

# Utiliser l'option --output.

En utilisant cette option vous aurez en résultat un nom de fichier de type _monfichier.current.csv_, mais si vous ne l'utilisez pas, vous obtiendrez un nom de fichier de type _DT-date.csv_.

# Mécanisme de l'option.

Selon le nom de fichier de sortie choisit, en mode "personnalisé" le script créera au maximum deux fichiers _current_ et _archive_, et en mode "date" le script créera un nouveau fichier à chaque exécution du script.

Pour utiliser cette option, indiquer un chemin de fichier sans extension.

**Bon tracking !**