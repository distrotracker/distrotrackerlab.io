---
title: "1 semaine après la sortie de Distro Tracker 1.0 : merci !"

layout: post
categories: news
date: 2024-07-23 17:07:00 +0200
lang: fr

author: Frédéric
---

Merci pour **votre enthousiasme** et **vos retours** quant à la sortie de Distro Tracker 1.0, vos commentaires sont très importants afin d’améliorer le programme.

Comme visible sur la carte ci-dessous, Distro Tracker est désormais utilisé dans plusieurs pays différents.

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

Une version "bugfix", contenant les réponses à vos observations, est en cours de réalisation, et elle sera publiée d’ici quelques jours.

En premier lieu il y aura **une nouvelle documentation**, suivant au plus près les standards en la matière, avec notamment l’ajout d’une page man en bonne et due forme. Il y aura également **l’ajout du format YAML** ainsi que la possibilité de télécharger les données générées par distrotracker.com, en lieu et place d’un tracking manuel.

Je me tournerais alors ensuite vers la réalisation d'un **paquet Flatpak** et d'un **paquet .deb** classique pour compléter la liste des modes de distribution !

**Bon tracking !**