---
title: "Nouvelle version Distro Tracker 1.1.0"

layout: post
categories: news
date: 2024-09-20 17:07:00 +0200
lang: fr

author: Frédéric
---

Distro Tracker évolue et passe en version 1.1.0 !

Voici les nouveautés de cette nouvelle version qui ajoute plusieurs fonctionnalités et corrige quelques bugs :

- Passage à **25 distributions** avec les nouvelles entrées suivantes : Linux (Kernel), Nextcloud (Server) & Rescuezilla.
- Nouvelle **documentation**, avec un fichier man respectueux des standards.
- Nouveau **filtre NOK** pour OPNsense, et nouvelles **URLs** pour openSUSE & Mageia.
- Ajout du **format YAML** en sortie, qui s'accompagne de l'ajout de l'option '--format' pour choisir entre CSV, JSON ou YAML - par défaut seul le CSV est en sortie.
- La valeur par défaut de l'option '--length' est maintenant à '3'.
- Toutes les options sont en minuscule.
- Les dates de révision dans les sorties JSON & YAML, et dans le report, sont au **format standard ISO 8601**.
- Correction du calcul du temps d'exécution.

Ce ne sont pas les seules nouveautés pour Distro Tracker puisque des changements ont lieu aussi sur le site internet :

- Mise en avant de la dernière actualité et du dernier rapport d'alerte sur l'accueil du site.
- Ajout d'une page "Roadmap" pour en savoir plus sur le développement de _distro-tracker_ et de ses utilitaires.
- Le site est désormais trilingue avec l'ajout d'une traduction espagnole !

Et enfin ajout du **format .deb (paquet Debian)** qui vient s'ajouter aux formats de distribution déjà existants (source Bash, binaire Bash & paquet Snapcraft).

Rejoignez **la communauté mondiale des utilisateurs de Distro Tracker** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Bon tracking !**