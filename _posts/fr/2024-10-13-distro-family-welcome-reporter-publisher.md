---
title: "Distro Reporter & Distro Publisher arrivent dans la famille !"

layout: post
categories: news
date: 2024-10-13 10:00:00 +0200
lang: fr

author: Frédéric
---

Distro Tracker accueille aujourd'hui deux petits frères : **Distro Reporter** & **Distro Publisher** !

Mais tout d'abord, **laissez-moi vous dire merci pour vos nombreux retours**, et grâce à vous, Distro Tracker a franchit la barre des 50 utilisateurs hebdomadaires selon les statistiques officielles Snapcraft. Merci à toutes & tous !

Dans Distro Tracker, la fonctionnalité de gestion des différences entre deux sets de données récoltées était **jusqu'à présent incluse directement dans Distro Tracker**, mais cela limitait les possibilités. Désormais, **vous êtes libres de programmer comme vous le souhaitez la comparaison entre des données**, et de générer des rapports aux formats JSON & YAML !

![distro-family]({{ site.url }}/assets/img/distro-family.png){: .img-fluid }

**Distro Reporter** est donc disponible dans **sa première version stable 1.0.0**, qui signifie chez moi l'ajout de toutes les fonctionnalités prévues au départ. Le script est disponible aux formats **source Bash**, **binaire Bash**, **paquet Debian** & **paquet Snap**. Vous trouverez tout sur mon Gitlab **[ici](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-reporter/prod/)**. La fonctionnalité sera prochainement retirée de Distro Tracker.

**Distro Publisher** est également en **première version stable 1.0.0** ! Il s'agit cette fois de pouvoir publier les rapports de changements générés par **Distro Reporter** dans différents logiciels ou réseaux sociaux. Ainsi, la boucle démarrant avec la récolte de données, puis l'analyse des changements et enfin la publication sur différents supports, est bouclée.

J'en profite donc pour vous annoncer également **l'ouverture de [la page Facebook de Distro Tracker](https://www.facebook.com/distrotracker)**, avec publications des rapports générés automatiquement et les posts d'actualités du site. N'hésitez pas à vous y abonner et liker !

D'autres outils consacrés à la gestion des contenus téléchargeables seront développés ultérieurement. Rendez-vous sur la page **[Roadmap]({{ site.url }}/fr/roadmap.html)** pour découvrir les futures évolutions prévues.

Et si ce n'est pas déjà fait : rejoignez **la communauté mondiale des utilisateurs de Distro Tracker** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Bon tracking !**