---
title: "Astuce : option --backup pour la sécurité des données"

layout: post
categories: news
date: 2024-07-17 18:07:00 +0200
lang: fr

author: Frédéric
---

# Utiliser l'option --backup.

En ajoutant l'option _--backup_ (ou _-b_ en notation simplifiée) à la commande, vous vous protégez contre un évènement inattendu indépendant de Distro Tracker : l’inaccessibilité du site internet d’une des distributions dont vous souhaitiez récupérer les informations.

# Mécanisme de l'option.

Selon le nom de fichier de sortie choisit, personnalisé ou basé sur la date, le script ira tester et retrouver les anciennes données les plus récemment conservées, et les remettra dans votre nouveau fichier de sortie.

# Traitement des données.

Grâce au champ _source_ présent dans tous les formats du fichier de sortie, le script vous notifie s’il s’agit de données nouvelles (_current_) ou issues de données précédentes (_archive_).

Ainsi vous pouvez toujours et en toutes circonstances constituer votre fichier de données !

**Bon tracking !**