---
title: "Distro Widget, pour toujours avoir Distro Tracker près de soi !"

layout: post
categories: news
date: 2024-10-20 10:00:00 +0200
lang: fr

author: Frédéric
---

Je vous propose aujourd'hui un nouvel outil simple, convivial et pratique : **Distro Widget** !

Il s'agit d'**une configuration spéciale pour [Conky](https://conky.cc/documents/about)**, le moniteur système qui s'affiche en arrière-plan sur le bureau, compatible avec les systèmes GNU/Linux & BSD.

Sa particularité ? **Elle intègre les données de Distro Tracker & Distro Reporter !** Et oui, pour la première fois je développe un outil à usage "desktop" en lien avec Distro Tracker.

Après une **installation simple**, **distro-get**, un petit utilitaire indépendant et léger, s'ajoute dans le répertoire `/home/user/.config/conky/` pour vous fournir deux fonctionnalités : le **téléchargement des données** (par défaut, celles mises à jour tous les matins sur mon Gitlab et au format JSON), et la **lecture de ces données**. La configuration Conky se trouve quant à elle dans le fichier `conky.conf`, aux côtés d'une image d'en-tête pour [Conky](https://conky.cc/documents/about). Un fichier `conky.desktop` est également crée dans le répertoire `/home/user/autostart/`.

Pour le moment, en plus de quelques informations système et statistiques, un affichage aléatoire d'une distribution avec la date et numéro de sa dernière version vous est proposé, ainsi que la liste des distributions ayant eu un changement dans le dernier rapport téléchargé.

### Installation.

```
wget https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-widget/dev/standalone/distro-widget.sh --no-check-certificate && chmod +x distro-widget.sh && ./distro-widget.sh --username "$USER"
```

### Configuration.

Téléchargement journalier des données distantes, par crontab par exemple, avec le format JSON par défaut :

```
0 13 * * * /home/test/.config/conky/distro-get.sh --action download --type tracker
0 13 * * * /home/test/.config/conky/distro-get.sh --action download --type reporter
```

### Exemple.

![distro-widget example]({{ site.url }}/assets/img/distro-widget.png){: .img-fluid }

N'hésitez pas à me faire part de vos retours. Un paquet Debian & Snapcraft seront réalisés prochainement.

**Bon tracking !**