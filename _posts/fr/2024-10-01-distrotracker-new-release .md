---
title: "Distro Tracker 1.1.1 - version corrective"

layout: post
categories: news
date: 2024-10-01 17:07:00 +0200
lang: fr

author: Frédéric
---

Distro Tracker passe aujourd'hui en 1.1.1, une version de correction pour les problèmes suivants :

- Correction de la **dépendance 'yq' (de Mike Farah) dans le paquet Snap**.
- Correction de l'expression régulière associée aux informations du **Kernel Linux**.
- Ajout d'une **protection supplémentaire** qui invalide tout résultat comportant deux entités dans la réponse.

Mettez à jour vos scripts, paquets Debian et Snap afin de profiter de ces corrections.

Restez à l'écoute car **dans quelques jours sortira _Distro Reporter_ en première version stable 1.0.0**, un "petit frère" pour _Distro Tracker_, afin d'analyser et gérer de manière indépendante les résultats sortis par _DT_. L'option '--report' sera alors supprimée et remplacé par ce tout nouveau membre de la famille.

Rejoignez **la communauté mondiale des utilisateurs de Distro Tracker** !

![distro-tracker map]({{ site.url }}/assets/img/map.png){: .img-fluid }

**Bon tracking !**