---
title: "Annonce officielle Distro Tracker 1.0"

layout: post
categories: news
date: 2024-07-17 17:07:00 +0200
lang: fr

author: Frédéric
---

# Distro Tracker 1.0 est là !

Après plusieurs semaines de développement et un changement d'identité, **voici venu la version 1.0 Distro Tracker**, l'outil de veille informatique qui permet de connaître les dernières versions stables de distributions GNU/Linux & BSD, avec leurs liens de téléchargement.

Tout d’abord et évidemment le plus visible, **la création d’un site web dédié : https://distrotracker.com**. Ce site, bilingue, comporte notamment une page de rendu HTML des données récoltées, un flux des changements de ces données, ainsi que des articles sur le fonctionnement détaillé du programme.

Vous allez y trouver le script Bash original mais aussi désormais **sous plusieurs modes de distribution** : un paquet Snap, un paquet Flatpak et un module PowerShell !

**Du côté des fonctionnalités, voici les nouveautés** :

- Passage à **22 distributions** avec les nouveaux entrants suivants : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, & XCP-ng.
- Mécanisme général de récupération des numéros de versions suivant le **paramètre _-LENGTH_** revu pour plus de logique : ce n'est pas juste X versions testées qui sont retournées mais dorénavant X versions valides.
- Ajout du **paramètre _-FILE_** pour choisir les URLs de tracking via un fichier JSON local ou distant.
- Ajout du **paramètre _-REPORT_** pour générer un rapport au format JSON pour notifier les changements éventuels sur la dernière release de chaque distribution.
- Activation du **paramètre _-ENV_**.
- Renommage des paramètres _-DISTRIB_ à _-DISTRO_, _-DEPTH_ à _-LENGTH_, _-DATASECURITY_ à _-BACKUP_.
- Ajout de raccourcis à une lettre sur chaque paramètre et option.
- Ajout d'une **fonction Compare_NokFilter** pour mieux gérer et expliciter les versions tagguées "NOK" (bêta, RC, etc) : toutes les versions pinguables sont récupérées, avec un dernier filtre sur les versions stables.

**Bon tracking !**