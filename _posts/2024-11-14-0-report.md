---
title: "2024-11-14"

layout: post
categories: changes
date: 2024-11-14 20:03:03 +0200

author: reporter
---

distribution linux - changes :
- last release upgrade from '6.11.7' to '6.11.8'
- last url from 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.11.7.tar.gz' to 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.11.8.tar.gz'

