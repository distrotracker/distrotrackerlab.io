---
title: "2024-09-01"

layout: post
categories: changes
date: 2024-09-01 16:44:23 +0200

author: reporter
---


distribution alma :
- last release from '9.3' to '9.4'


distribution debian :
- last release from '12.6.0' to '12.7.0'


