---
title: "2024-12-09 (Linux Kernel, Manjaro Linux)"

layout: post
categories: changes
date: 2024-12-09 12:03:08 +0200

author: reporter
---

distribution Linux Kernel - changes :
- last release upgrade from '6.12.3' to '6.12.4'
- last url from 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.12.3.tar.gz' to 'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.12.4.tar.gz'

distribution Manjaro Linux - changes :
- last url from 'https://download.manjaro.org/kde/24.2.0/manjaro-kde-24.2.0-241208-linux612.iso' to 'https://download.manjaro.org/kde/24.2.0/manjaro-kde-24.2.0-241209-linux612.iso'

