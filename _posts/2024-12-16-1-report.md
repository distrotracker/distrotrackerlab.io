---
title: "2024-12-16 (Manjaro Linux)"

layout: post
categories: changes
date: 2024-12-16 20:03:23 +0200

author: reporter
---

distribution Manjaro Linux - changes :
- last release downgrade from '24.2.1' to '24.2.0'
- last url from 'https://download.manjaro.org/kde/24.2.1/manjaro-kde-24.2.1-241216-linux612.iso' to 'https://download.manjaro.org/kde/24.2.0/manjaro-kde-24.2.0-241209-linux612.iso'

