---
title: Acerca de

layout: page
permalink: /about.html
lang: es

author: Frédéric
---

## ¿Qué es _Distro Tracker_?

La herramienta de monitoreo de TI para conocer las últimas versiones estables de distribuciones GNU/Linux y BSD, con sus enlaces de descarga.

## Distribuciones compatibles.

{% include distro-list.md %}

## Licencia.

El script y el sitio web están bajo GPL-3.0 o posterior.

## Quien soy ?

Soy Frédéric Petit, soy un profesional de TI radicado en Francia. Puedes descubrir mi trabajo en [fredericpetit.fr](https://fredericpetit.fr).

## Contactar.

Utilice la dirección de correo electrónico < contact AT fredericpetit.fr > para ponerse en contacto conmigo.