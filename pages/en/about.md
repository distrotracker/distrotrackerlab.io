---
title: About

layout: page
permalink: /about.html
lang: en

author: Frédéric
---

## What is _Distro Tracker_ ?

The IT monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions, with their download links.

## Supported distributions.

{% include distro-list.md %}

## License.

Script & website are under GPL-3.0-or-later.

## Who i am ?

I'm Frédéric Petit, I'm an IT professional based in France. You can discover my work at [fredericpetit.fr](https://fredericpetit.fr).

## Contact.

Use the email address < contact AT fredericpetit.fr > to contact me.