---
title: Interviews

layout: page
permalink: /interviews/
lang: fr

author: Frédéric
---

# Explication du projet.

Professionnel de l'informatique mais également passionné de par mon engagement associatif passé via l'organisation d'événements ou la réalisation de projets en ligne, l'aspect sociologique de l'informatique est important pour moi. Je trouve ainsi intéressant de découvrir quelles sont les raisons ou l'histoire personnelle qui ont poussés quelqu'un à réaliser quelque chose.

Je propose donc à tout-e utilisateur-ice de Distro Tracker de répondre à ces quelques questions, en limitant à environ 100 mots maximum par question.

1. Présentation professionnelle, parcours scolaire et découverte de l'informatique.
1. Premiers pas dans le monde du logiciel libre.
1. Raisons pour lesquelles adopter Distro Tracker comme outil de veille.
1. Vision générale du monde de l'informatique aujourd'hui, entre dangers, utilités et innovations.
1. Publicité personnelle et projets favoris.