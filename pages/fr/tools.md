---
title: Outils

layout: page
permalink: /tools.html
lang: fr

author: Frédéric
---

Retrouvez le développement sur mon **[Gitlab](https://gitlab.com/fredericpetit/self/)**.


## 💻 Distro Tracker - ✅ current 1.2.0

### Présentation.

L'outil principal de ce projet, un outil de suivi des distributions GNU/Linux & BSD, permettant de connaitre leurs dernières versions, avec leurs liens de téléchargement, directement depuis les sites internets de celles-ci. Disponible sous format bash source, paquet Debian et paquet Snapcraft.

### Développement.

Il est prévu la création d'un paquet Flatpak, prendre en charge CSV & YAML pour le stockage externe des URLs à traquer, fichiers produits par _distro-url_, une optimisation sur les tests NOK & Ping, ainsi qu'une réorganisation des URLs des distributions en tableau indicé (plus de limitation à 2). Une version Python est également à l'étude pour une plus grande flexibilité et efficacité dans la gestion des requêtes.


### 💻 Distro URL - ✅ current 1.1.0

### Présentation.

Utilitaire qui génére des fichiers d'URLs à parser, pour l'option '--source' de _distro-tracker_. Disponible sous format bash source.

### Développement.

Dans le futur, je prévois une réorganisation des URLs des distributions en tableau indicé (plus de limitation à 2).


### 💻 Distro Converter - ✅ current 1.0.0

### Présentation.

Utilitaire qui analyse les résultats de _distro-tracker_ pour convertir les données vers et depuis les formats HTML, Markdown, CSV, YAML & JSON. Disponible sous format bash source.

### Développement.

Pour les prochaines versions, ne pas se contenter de convertir le CSV vers HTML, Markdown, YAML & JSON, mais ajouter petit à petit toutes les autres possibilités.


### 💻 Distro Reporter - ✅ current 1.1.0

### Présentation.

Utilitaire qui analyse les résultats de _distro-tracker_ pour générer des rapports de changements entre anciennes et nouvelles données. Disponible sous format bash source, paquet Debian et paquet Snapcraft.

### Développement.

Il n'y a pour le moment pas de gros travaux prévus sur cet utilitaire.


### 💻 Distro Publisher - ✅ current 1.1.0

### Présentation.

Utilitaire qui analyse les rapports de _distro-reporter_ pour les publier sur différents supports. Disponible sous format bash source.

### Développement.

Explorer la production multimédia.


## 💻 Distro Widget - ✅ current 1.1.0

### Présentation.

Affiche des informations de Distro Tracker dans un widget Conky, télécharge _distro-downloader_ et _distro-reader_ qui permettent de récupérer les données de Distro Tracker (depuis _gitlab.org_ ou _distrotracker.com_), et de les lire. Disponible sous format bash source.

Pour l'installer, exemple sous Ubuntu 22.04 LTS pour un utilisateur "user" :

```
wget https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-widget/dev/standalone/distro-widget.sh --no-check-certificate && chmod +x distro-widget.sh && sudo ./distro-widget.sh --username "user";
```

Deux services _systemd_ sont créés automatiquement pour assurer les mises à jour.


### Développement.

Réflexions sur un usage multi-utilisateurs, avec un binaire commun dans _/usr/bin_.


## ⚠️ Distro Manager - 🚧 dev

### Présentation.

Gérer une banque d'ISO via les données de _distro-tracker_. En développement !


## ⚠️ Distro Sourcer - 🚧 dev

### Présentation.

Utilitaire pour gérer les dépôts. En développement !


## ⚠️ Distro Dashboard - 🚧 dev

### Présentation.

Une visualisation graphique des données de _distro-tracker_. En développement !