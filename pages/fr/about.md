---
title: À propos

layout: page
permalink: /about.html
lang: fr

author: Frédéric
---

## Qu'est-ce que _Distro Tracker_ ?

L’outil de veille informatique qui permet de connaître les dernières versions stables de distributions GNU/Linux & BSD, avec leurs liens de téléchargement.

## Distributions prises en charge.

{% include distro-list.md %}

## Licence.

Script & site web sont sous GPL-3.0-or-later.

## Qui suis-je ?

Je m'apelle Frédéric Petit, je suis un professionnel de l'informatique exerçant en France, vous pouvez découvrir mes travaux sur [fredericpetit.fr](https://fredericpetit.fr).

## Contact.

Utilisez l'adresse email < contact AT fredericpetit.fr > pour me contacter.