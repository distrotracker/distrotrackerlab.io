---
title: Doc

layout: page
permalink: /documentation.html

author: Frédéric

syntax: highlight
---

<pre>



NAME



	distro-tracker - The IT monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions.





REQUIREMENTS



	program 'wget' (mandatory)



 	program 'jq' (mandatory)



 	program 'yq' (mandatory)



 

SYNOPSIS



	distro-tracker [ OPTIONS ]





DESCRIPTION



	This open-source tool analyzes remote content using multiple regular expressions and standardizes the output into CSV, JSON, & YAML files.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path or single distant url to JSON data source for distributions URLs.

 		- state : optionnal.

 		- accepted value(s) : path. example at 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json'. CSV & YAML formats available in a next release.

 		- default value : no default, hardcoded URLs are the source until this option is provisioned.



 	-d NAMES,

	--distro NAMES

 		Single distribution name or coma separated list, without spaces, of distributions names.

 		- state : optionnal.

 		- accepted value(s) : available distributions names are 'alma', 'alpine', 'arch', 'centos', 'clonezilla', 'debian', 'fedora', 'freebsd', 'kaisen', 'kali', 'linux', 'mageia', 'manjaro', 'mint', 'mx', 'nextcloud', 'opensuse', 'opnsense', 'parrot', 'pfsense', 'proxmox', 'rescuezilla', 'slackware', 'ubuntu' & 'xcpng', or 'all' registered.

 		- default value : 'all'.



 	-c (no type),

	--cli (no type)

 		Force CLI instead GUI desktop in ISO distribution if available.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-l NUMBER,

	--length NUMBER

 		Number of valid releases by distribution needed.

 		- state : optionnal.

 		- accepted value(s) : any number starting from 1.

 		- default value : '3'.



 	-o FILEPATH,

	--output FILEPATH

 		Single file path without extension to store the final result. If provisioned it's custom name mode, if not it's date name mode.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/DT-DATETIME.FORMAT' (date name mode).



 	-f EXTENSION,

	--format EXTENSION

 		Single data format for previous '--output' option.

 		- state : optionnal.

 		- accepted value(s) : 'csv', 'json', 'yaml' or 'all'.

 		- default value : 'csv'.



 	-r (no type),

	--report (no type)

 		Generate a JSON report file, only if new releases detected, in same '--output' option, into a file named '*.report.json'.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-b (no type),

	--backup (no type)

 		Backup security for keeping and don't removing previous data if entire website checking fails. New and old datas need to be in the same directory. CSV & YAML formats available in a next release.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-g (no type),

	--get (no type)

 		Download data from the site https://distrotracker.com/. You can specify '--output' & --format' options. No other option will be considered.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : default is 'https://distrotracker.com/datas.csv'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	all distributions in quiet mode :

		distro-tracker -q



 	custom distributions, custom length, custom output, with backup :

		distro-tracker -d debian,fedora,freebsd,mageia,slackware -l 10 -o distro-tracker -b



 	mageia distribution in JSON format with custom URLs :

		distro-tracker -d mageia -f json -s https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json



 	get generated scheduled data from distrotracker.com in YAML format :

		distro-tracker -g -f yaml



 

TO-DO



	Flatpak package, CSV & YAML formats for distributions URL.





NOTES



	'yq' dependencie is 'yq' by Mike Farah.



 	Some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.



 	Alma, CentOS, Fedora & MX have a main index where everything is listed, but some versions are downloaded from another place.



 	Fedora lists in the index of a release an 'OSB' version, a version built with osbuilder.



 	Alma and CentOS can list 'non-existent' versions.



 	Alma, Fedora & Ubuntu have both ui or cli mode.



 	Alpine & XCP-ng nead a sort with head because many releases listed in single release page.



 	Nextcloud & Rescuezilla use github, so the NOK filter is applied when listing all versions for ping limitation issues by Github servers.



 

CREDITS & THANKS



	x





CHANGELOG



	1.1.1 - 2024-10-01

		Fix **Linux Kernel** regex. Add additional protection that invalidates any result with two entities in the response.



 	1.1.0 - 2024-09-19

		Increase to 25 distributions with the following new entries: Linux (Kernel), Nextcloud (Server) & Rescuezilla. New documentation, with a standards-compliant man file. New NOK filter for OPNsense, and new URLs for openSUSE & Mageia. Added YAML output format, which is accompanied by the addition of the '--format' option to choose between CSV, JSON or YAML - by default only CSV is output. The default value of the '--length' option is now '3'. All options are lowercase. The revision dates in the JSON & YAML output, and in the report, are in standard ISO 8601 format. Fixed execution time calculation.



 	1.0.0 - 2024-07-17

		Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the '-LENGTH' option has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the '-SOURCE' option to choose tracking URLs via a local or remote JSON file. Addition of the '-REPORT' option to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the '-ENV' option. Renaming of the '-DISTRIB' option to '-DISTRO', '-DEPTH' to '-LENGTH', '-SECURITY' to '-BACKUP'. Addition of one-letter shortcuts for each option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged 'NOK' (beta, RC, etc.): all pingable versions are retrieved, with a final filter on stable versions.



 	0.5.0 - 2024-06-01

		Add -SECURITY option for copy old datas to new datas if wget fails on entire website.



 	0.4.0 - 2024-06-01

		Up to 15 distributions by default.



 	0.3.0 - 2024-06-01

		New strong REGEXP. Add '-LENGTH' option. Add command JQ requirement. Add JSON output. Up to 12 distributions GNU/Linux & BSD.



 	0.2.0 - 2024-06-01

		Separate checks on index page & release page.



 	0.1.0 - 2024-06-01

		Add CSV output.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.1.1

	License : GPL-3.0-or-later





</pre>