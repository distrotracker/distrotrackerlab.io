# VERBATIM.

`Model : 2023-04-13-self.`

| param | value |
| ------ | ------ |
| **license** | Choose license from [spdx.org/licenses](https://spdx.org/licenses/). |
| **tokens** | x |
| **CI/CD variables** | Add `CUSTOM_EMAIL` (Protected, Masked, Expanded), `CUSTOM_NAME` (Protected, Expanded), `GITLAB_TOKEN` (Protected, Masked, Expanded), `BLUESKY_TOKEN` (Protected, Masked, Expanded) and `FACEBOOK_TOKEN` (Protected, Masked, Expanded). |
| **CI/CD files** | Create _./.gitlab/default-ci.yml_ based on '_Self_' home, and _./gitlab-ci.yml_. |
| **Composer** | x |
| **pipeline trigger** | Actions "build" / "deploy" / "build-deploy" keywords in commit. Source "web" do "build-deploy" action. |
| **pipeline schedule** | "build-deploy" do "build-deploy" action 12h00 everyday. |