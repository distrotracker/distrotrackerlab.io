// Service Worker version.
const PREFIX = "V4";

// Cache.
const BASE = location.protocol + "//" + location.host;
const CACHED_FILES= [
//	`${BASE}/datas.json`,
	`${BASE}/assets/vendor/twbs/bootstrap/dist/css/bootstrap.min.css`,
	`${BASE}/assets/vendor/fortawesome/font-awesome/css/all.min.css`,
	`${BASE}/assets/vendor/fredericpetit/ubuntu-font-composer/assets/css/font.min.css`,
	`${BASE}/assets/vendor/fredericpetit/highlightjs-composer/assets/css/xt256.css`
]

// Listen installation.
self.addEventListener('install', (event) => {
	self.skipWaiting();
	event.waitUntil(
		(async () => {
			const cache = await caches.open(PREFIX);
			await Promise.all(
				[...CACHED_FILES, "/offline.html"].map((path) => {
					return cache.add(new Request(path));
				})
			);
		})()
	);	
	console.log(`DT :: ${PREFIX} Install.`);
});

// Listen activate.
self.addEventListener('activate', (event) => {
	clients.claim();
	event.waitUntil(
		(async() => {
			const keys = await caches.keys();
			await Promise.all(
				keys.map((key) => {
					if (!key.includes(PREFIX)) {
						return caches.delete(key);
					}
				})
			);
		})()
	);
	console.log(`DT :: ${PREFIX} Active.`);
});

// Listen fetch.
self.addEventListener("fetch", (event) => {
	console.log(`DT :: ${PREFIX} Fetching : ${event.request.url}, Mode : ${event.request.mode}.`)
	if (event.request.mode == "navigate") {
		event.respondWith(
			(async () => {
				try {
					const preloadResponse = await event.preloadResponse;
					if (preloadResponse) {return preloadResponse;}
					return await fetch(event.request);
				} catch (e) {
					const cache = await caches.open(PREFIX);
					return await cache.match("/offline.html");
				}
			})()
		);
	} else if(CACHED_FILES.includes(event.request.url)) {
		event.respondWith(caches.match(event.request));
	}
});