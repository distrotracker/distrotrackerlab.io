# [EN] Distro Tracker - find out the latest versions of GNU/Linux & BSD distributions.
# [FR] Distro Tracker - connaître les dernières versions de distributions GNU/Linux & BSD.

![logo](assets/img/logo_460.png){height=111px}

_Powered by Jekyll._

| param | value |
| ------ | ------ |
| **URL git distrotracker** | [gitlab.com/distrotracker/distrotracker.gitlab.io](https://gitlab.com/distrotracker/distrotracker.gitlab.io/) |
| **URL website** | [distrotracker.com](https://distrotracker.com/) |
| **NAMESPACE**<br />(Snapcraft) | [distro-tracker](https://snapcraft.io/distro-tracker) |
| **NAMESPACE**<br />(Flathub) | [com.distrotracker.distro-tracker](https://flathub.org/apps/com.distrotracker.distro-tracker) |
| **SCHEDULE**<br />(pipeline) | 'build-deploy' action 12h00 everyday |

## Usage.
- Scheduled execution with [Distro Tracker script](https://gitlab.com/fredericpetit/self).