//############################
//# by : fredericpetit.fr
//############################
//# script : main.js
//# usage : main script
//############################

// #################### https location ####################
// gitlab pages doesn't support redirect www to no-www.
if (window.location.host.startsWith("www.")) {
	window.location.replace(window.location.href.replace("www.", ""))
}