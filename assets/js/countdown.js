//############################
//# by : fredericpetit.fr
//############################
//# script : countdown.js
//############################

// #################### countdown index page ####################
// show next update.
function updateCountdown() {
    // init.
    const now = new Date();
    const nextNoon = new Date();
    const nextEvening = new Date();
    // hours.
    nextNoon.setHours(12, 0, 0, 0);
    nextEvening.setHours(20, 0, 0, 0);
    // define next check.
    const target = now < nextNoon ? nextNoon : (now < nextEvening ? nextEvening : new Date(nextNoon.getTime() + 86400000));
    const diff = target - now;
    // set counts.
    const hours = Math.floor(diff / (1000 * 60 * 60));
    const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((diff % (1000 * 60)) / 1000);
    // html.
    document.querySelector("span#countdown").textContent = `${hours.toString().padStart(2, '0')}h ${minutes.toString().padStart(2, '0')}m ${seconds.toString().padStart(2, '0')}s`;
    setTimeout(updateCountdown, 1000);
}